<?php
use Migrations\AbstractMigration;

class Projects extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('projects');
        $table->addColumn('description', 'string', [
            'default' => null,
            'limit' => 6000,
            'null' => true,
        ]);
        $table->addColumn('distribution', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => true,
        ]);
        $table->addColumn('hours', 'string', [
            'default' => null,
            'limit' => 50,
            'null' => true,
        ]);
        $table->create();
    }
}
