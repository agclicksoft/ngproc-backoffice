<?php
use Migrations\AbstractMigration;

class Evaluations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('evaluations');
        $table->addColumn('parter_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ]);
        $table->addColumn('client_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ]);
        $table->addColumn('cotation_id', 'integer', [
            'default' => null,
            'null' => false,
            'limit' => 11,
        ]);

        $table->addColumn('value', 'integer', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('owner', 'string', [
            'default' => null,
            'limit' => 40,
            'null' => false,
        ]);
        $table->addForeignKey('cotation_id', 'cotations', 'id');
        $table->addForeignKey('parter_id', 'users', 'id');
        $table->addForeignKey('client_id', 'users', 'id');
        $table->create();
    }
}
